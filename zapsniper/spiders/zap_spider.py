
import scrapy
import base64
import json

from scrapy_splash import SplashRequest


class ZapSpider(scrapy.Spider):
    name = "zap"
    allowed_domains = ['www.zapimoveis.com.br']

    def __init__(self,
                 count=None, *args, **kwargs):
        super(ZapSpider, self).__init__(*args, **kwargs)

        self.count = int(count) if count else None

        self.crawl_count = 0
        self.scrape_count = 0
        self.total_crawl = 0
        self.total_scrape = 0

        self.lua_script = """
            function main(splash)
                splash.response_body_enabled = true
                assert(splash:go(splash.args.url))
                assert(splash:runjs([[
                        p=$('input[name="txtPaginacao"]');
                        p.val({pag});
                        p.blur();
              ]]))

              assert(splash:wait(4))
              return {{har=splash:har()}}
            end
        """

    def start_requests(self):
        urls = [
            "https://www.zapimoveis.com.br/aluguel/apartamentos/sp+sao-paulo+zona-oeste+pinheiros",
            "https://www.zapimoveis.com.br/aluguel/apartamentos/sp+sao-paulo+centro+higienopolis/",
            "https://www.zapimoveis.com.br/aluguel/apartamentos/sp+sao-paulo+zona-oeste+sumare/",
            "https://www.zapimoveis.com.br/aluguel/apartamentos/sp+sao-paulo+zona-oeste+sumarezinho/"
        ]

        lua_script = """
            function main(splash)
                splash.response_body_enabled = true
                assert(splash:go(splash.args.url))

                assert(splash:wait(3))

                assert(splash:runjs([[
                      p=$('input[name="txtPaginacao"]');
                      p.val(2);
                      p.blur();
                ]]))

                assert(splash:wait(3))

                assert(splash:runjs([[
                      p=$('input[name="txtPaginacao"]');
                      p.val(1);
                      p.blur();
                ]]))

                assert(splash:wait(3))

                return {har=splash:har()}
            end
        """

        for url in urls:
            yield SplashRequest(url, self.parse_result, endpoint='execute',
                                args={'lua_source': lua_script},
                                dont_filter=True)

    def parse_realty(self, raw_data):
        zap_id = raw_data["ZapID"]

        rooms = raw_data["QuantidadeQuartos"]

        raw_cond = raw_data["PrecoCondominio"][3:].replace(".", "")
        cond = int(raw_cond) if len(raw_cond) > 0 else 0

        raw_price = raw_data["Valor"][3:].replace(".", "")
        self.log("Raw Price: <{}>".format(raw_price))
        price = int(raw_price) + cond

        url = raw_data["UrlFicha"]

        desc = raw_data['Observacao']
        amenities = u", ".join(raw_data["Caracteristicas"])

        realty = {
            "id": zap_id,
            "rooms": rooms,
            "price": price,
            "url": url,
            "desc": desc,
            "amenities": amenities
        }

        return realty

    def parse_result(self, response):
        response_data = response.data

        data = filter(lambda x: "RetornarBuscaAssincrona" in x['request']
                      ['url'], response_data['har']['log']['entries'])

        data = list(data)
        self.log("tamanho lista: <{}>".format(len(data)))
        base64_raw_data = data[0]["response"]["content"]["text"]
        raw_data = json.loads(base64.b64decode(base64_raw_data))

        total_pages = raw_data["Resultado"]["QuantidadePaginas"]
        current_page = raw_data["Resultado"]["PaginaAtual"]

        if current_page == 2:
            if self.count:
                if self.count == 1:
                    to_page = 1
                else:
                    to_page = self.count
            else:
                to_page = total_pages

            self.log('Crawling {} of {} listing pages...'.format(to_page, total_pages))

            for pag in range(3, to_page + 1):
                yield SplashRequest(response.url, self.parse_result,
                                    endpoint='execute',
                                    args={'lua_source': self.lua_script.format(pag=pag)},
                                    dont_filter=True)

        raw_realties = raw_data["Resultado"]["Resultado"]
        realties = map(self.parse_realty, raw_realties)

        self.log('**** Scraped: {}/{}'.format(current_page, total_pages))

        for realty in realties:
            yield realty

    def parse(self, response):
        self.log("Calling parse")
