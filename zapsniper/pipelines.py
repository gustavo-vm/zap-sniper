# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


# class ZapsniperPipeline(object):
#     def process_item(self, item, spider):
#         return item

from scrapy.exceptions import DropItem

import telegram
import json

# {
#     "id": zap_id,
#     "rooms": rooms,
#     "price": price,
#     "url": url,
#     "desc": desc,
#     "amenities": amenities
# }

class ExpensiveRealtyPipeline(object):

    def __init__(self):
        self.max_price = 2800
        self.rooms = 2

    def process_item(self, item, spider):
        item_price = item['price']

        if item_price > self.max_price:
            raise DropItem("Too expensive: {}".format(item))
        elif item["rooms"] < self.rooms:
            raise DropItem("Not enough rooms: {}".format(items))
        else:
            return item


class DuplicateRealtyPipeline(object):

    def __init__(self):
        self.db = {}

    def open_spider(self, spider):
        try:
            with open('zap.json', 'r') as f:
                self.db = json.load(f)
        except OSError:
            pass

    def close_spider(self, spider):
        with open('zap.json', 'w') as f:
            json.dump(self.db, f, sort_keys=True, indent=2)

    def process_item(self, item, spider):
        item_id = item["id"]

        if item_id in self.db:
            if item["price"] != self.db[item_id]["price"]:
                self.db[item_id] = item
                return item
            else:
                raise DropItem("Item already scrapped")
        else:
            self.db[item_id] = item
            return item


class TelegramPipeline(object):

    def __init__(self):
        self.bot = None

    def open_spider(self, spider):
        self.bot = telegram.Bot(token='587814782:AAE_aAo-kZsvXb_nEipRRY3ZWX0S7H2cTlw')

    def close_spider(self, spider):
        pass

    def process_item(self, item, spider):
        text = "Novo apartamento encontrado!\nPreço:\t{price}\nQuartos:\t{rooms}\nDescrição:\t{desc}\nAmenities:{amenities}\nlink:\t{url}".format(**item)
        self.bot.send_message(chat_id="793039437", text=text)
